from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .views import bookFinder
from .apps import BookfinderConfig

# Create your tests here.
class UnitTest(TestCase):
    def test_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_book_finder_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, bookFinder)

    def test_app_name(self):
        self.assertEqual(BookfinderConfig.name, 'BookFinder')
        self.assertEqual(apps.get_app_config('BookFinder').name, 'BookFinder')