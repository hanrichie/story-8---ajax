from django.apps import AppConfig


class BookfinderConfig(AppConfig):
    name = 'BookFinder'
