from django.urls import path
from . import views

app_name = 'BookFinder'

urlpatterns = [
    path('', views.bookFinder, name='bookFinder'),
]