$(document).ready(function(){
    $('form').submit(function(event){
        let item = $('input').val();
        let url = "https://www.googleapis.com/books/v1/volumes?q=" + item;

        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
            success: function(result){
                let dataList = result.items;
                $('tbody').empty();
                
                for (i = 0; i < dataList.length; i++) {
                    let data = dataList[i].volumeInfo;

                    var number = $('<td>').text(i+1);
                    var image = $('<td>').append($('<img>').attr({
                        'src': data.imageLinks.smallThumbnail
                        }));
                    var name = $('<td>').text(data.title);
                    var author = $('<td>').text(data.authors);

                    var bookData = $('<tr>').append(number, image, name, author);
                    $('tbody').append(bookData);

                    if (i >= 20) {
                        break;
                    }
                }
            },
        })
        event.preventDefault();
    })
});